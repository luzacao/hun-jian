// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  console.log('调用云函数')
  let data = {}
  const infoCol = db.collection('baseinfo').doc("17453ede609b2f50091e5bc6622551b0")
  data = await infoCol.get()
  return data;
}