let timer = 0
let interval = 1000
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    target: {
      type: String,
      value: "2020/12/18 08:00:00"
    },
    showDay: {
      type: Boolean,
      value: true
    },
    callback: {
      type: String,
      value: ""
    },
    clearTimer: {
      type: Boolean,
      value: true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    day: "00",
    hou: "00",
    min: "00",
    sec: "00",
    lastTime: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 根据剩余时间戳，获取天、时、分、秒
    formatCountDown(time) {
      const days = 24 * 60 * 60 * 1000
      const hours = 60 * 60 * 1000
      const minutes = 60 * 1000
      let day = Math.floor(time / days)
      let hou = Math.floor((time - day * days) / hours)
      let min = Math.floor((time - day * days - hou * hours) / minutes)
      let sec = Math.floor((time - day * days - hou * hours - min * minutes) / 1000)
      day = this.timeFormin(day)
      hou = this.timeFormin(hou)
      min = this.timeFormin(min)
      sec = this.timeFormin(sec)
      this.setData({
        day: this.timeFormat(day),
        hou: this.timeFormat(hou),
        min: this.timeFormat(min),
        sec: this.timeFormat(sec)
      })
    },

    // 小于10的格式化函数（2变成02）
    timeFormat(param) {
      return param < 10 ? '0' + param : param
    },

    // 小于0的格式化函数（不会出现负数）
    timeFormin(param) {
      return param < 0 ? 0 : param
    },

    // 定时器
    tick() {
      let {
        lastTime
      } = this.data
      timer = setTimeout(() => {
        if (lastTime < interval) {
          // 剩余时间小于1秒处理
          clearTimeout(timer)
          this.setData({
            lastTime: 0
          }, () => {
            this.formatCountDown(lastTime)
            this.onEnd()
          })
        } else {
          lastTime -= interval;
          this.setData({
            lastTime
          }, () => {
            this.formatCountDown(lastTime)
            this.tick()
          })
        }
      }, interval)
    },

    // 初始化时间
    initTime(target) {
      const nowTime = new Date().getTime() // 现在时间（时间戳）
      const endTime = new Date(target.replace(/-/g, '/')).getTime() // 结束时间（时间戳）
      const lastTime = endTime - nowTime // 距离结束的毫秒数
      return {
        lastTime: lastTime < 0 ? 0 : lastTime
      }
    },

    // 时间结束时回调事件
    onEnd() {
      this.triggerEvent("callback", {})
    }
  },

  lifetimes: {
    attached() {
      // 根据 target 初始化组件的lastTime属性
      this.setData({
        lastTime: this.initTime(this.properties.target).lastTime
      }, () => {
        // 界面更新渲染之后的回调
        // 开启定时器
        this.tick()
      })
    },

    // 组件销毁时清除定时器
    detached() {
      clearTimeout(timer)
    }
  }
})