App({
  globalData: {},

  onLaunch() {
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        // env 参数说明：
        //   env 参数决定接下来小程序发起的云开发调用（wx.cloud.xxx）会默认请求到哪个云环境的资源
        //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
        //   如不填则使用默认环境（第一个创建的环境）
        env: 'dev-0gvrrg8192ed8dad',
        traceUser: true,
      })
    }

    // 预拉取数据，需小程序后台配置
    wx.getBackgroundFetchData({
      fetchType: 'pre',
      success: (res) => {
        this.globalData.baseinfo = JSON.parse(res.fetchedData).data
      }
    })

    // 全局音乐
    const backgroundAudioManager = wx.getBackgroundAudioManager();
    // backgroundAudioManager.title = "告白气球";
    // backgroundAudioManager.singer = "周杰伦";
        // backgroundAudioManager.title = this.globalData.baseinfo.media.title;
    // backgroundAudioManager.singer = this.globalData.baseinfo.media.singer;
    this.globalData.bgMusic = backgroundAudioManager;
    this.globalData.bgMusicPlaying = true;
  },

  onShow() {
    !this.globalData.bgMusicPlaying && this.globalData.bgMusic.play();
    this.globalData.bgMusicPlaying = true;
  },

  onHide() {
    this.globalData.bgMusicPlaying && this.globalData.bgMusic.pause();
    this.globalData.bgMusicPlaying = false;
  },

  // 获取基本的全局信息
  async getBaseInfo() {
    try {
      const db = wx.cloud.database({
        env: wx.DYNAMIC_CURRENT_ENV
      })
      const {
        data
      } = await db.collection("baseinfo").doc("17453ede609b2f50091e5bc6622551b0").get()
      return data
    } catch {
      // do catch
      console.log("error")
    }
  }
})