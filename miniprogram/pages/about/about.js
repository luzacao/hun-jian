const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    baseinfo: {},
    versionNo: '2.0.0',
  },

  showWeChatNo() {
    console.log(this.baseinfo);
    wx.previewImage({
      urls: [this.data.baseinfo.weChatNoUrl],
    })
  },

  showCodeImg() {
    wx.previewImage({
      urls: [this.data.baseinfo.miniProgramUrl],
    })
  },

  shareProgram() {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },

  reloadProgram() {
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const {
      baseinfo
    } = app.globalData;
    this.setData({
      baseinfo: baseinfo || {}
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '诚邀参加我的婚礼',
      path: '/pages/index/index',
      imageUrl: this.data.baseinfo.shareCoverImg,
    }
  },

  onShareTimeLine() {
    return {
      title: '诚邀参加我的婚礼',
      imageUrl: this.data.baseinfo.shareCoverImg,
    }
  }
})